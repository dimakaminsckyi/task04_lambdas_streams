package com.epam.controller;

public interface TaskController {

    void runFirstTask();

    void runThirdTask();

    void runFourthTask();
}
